﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UberVguhStatistics.Interfaces;

namespace UberVguhStatistics.UberVguhStatisticsModel
{
  public  class AuthModel : IAuthModel
   {
        private readonly ICookieHelper _cookieHelper;
        private readonly IUberClientHelper _uberClient;
        public AuthModel(IUberClientHelper uberClient, ICookieHelper cookieHelper)
        {
            _uberClient = uberClient;
            _cookieHelper = cookieHelper;
        }
        public string GetAuthorizedUrl(List<string> scopes)
        {
            //var scopes = new List<string> { "history","profile+"};
            string redirectUrl = null;
            var uberAuthenticationClient = _uberClient.GetAuth();
            var response = uberAuthenticationClient.GetAuthorizeUrl(scopes, null, redirectUrl);
            return response;
        }
        public async Task<IActionResult> GetAuthotizedCallBack(string code)
        {
            var uberAuthenticationClient = _uberClient.GetAuth();
            var accessToken = await uberAuthenticationClient.GetAccessTokenAsync(code, "http://localhost:65463//auth/callback");
            if (accessToken == null || string.IsNullOrWhiteSpace(accessToken.Value))
            {
               // return RedirectToAction("Index");
            }
            _cookieHelper.SetAccessToken(accessToken);
            return null;
        }
    }
}
