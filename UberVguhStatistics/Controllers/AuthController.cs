﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UberVguhStatistics.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UberVguhStatistics.Web.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly IAuthModel _authModel;
        public AuthController(IAuthModel authModel)
        {
            _authModel = authModel;
        }

        [Route("")]
        [HttpGet("GetAuth")]      
        public IActionResult GetAuth()
        {
            var scopes = new List<string> { "history+", "profile+" };
            var response = _authModel.GetAuthorizedUrl(scopes);
            return RedirectToAction("GetCallBack", "Auth", response);
        }

        [HttpGet("GetCallBack")]
        public async Task<IActionResult> GetCallBack(string code)
        {
          var res =  await _authModel.GetAuthotizedCallBack(code);
            return  res;

        }



    }
}
