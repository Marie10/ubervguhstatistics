﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace UberVguhStatistics.Interfaces
{
   public interface IAuthModel
    {
        string GetAuthorizedUrl(List<string> scopes);
        Task<IActionResult> GetAuthotizedCallBack(string code);
    }
}
