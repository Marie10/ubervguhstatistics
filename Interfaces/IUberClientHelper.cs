﻿using System;
using System.Collections.Generic;
using System.Text;
using Uber.SDK;

namespace UberVguhStatistics.Interfaces
{
  public interface IUberClientHelper
    {
         UberAuthenticationClient GetAuth();
         UberSandboxClient Get(string clientToken = null);
    }
}
