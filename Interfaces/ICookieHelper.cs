﻿using Microsoft.AspNetCore.Http;

using Uber.SDK.Models;

namespace UberVguhStatistics.Interfaces
{
   public interface ICookieHelper
    {
         AccessToken GetAccessToken();
         AccessToken GetAccessTokenTest();
         void SetAccessToken(AccessToken accessToken);
         void SetAccessTokenTest(AccessToken accessToken, HttpResponse Response);

    }
}
