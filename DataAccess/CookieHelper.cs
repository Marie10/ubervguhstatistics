﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Uber.SDK.Models;
using UberVguhStatistics.Interfaces;

namespace berVguhStatistics.DataAccess
{
   public  class CookieHelper: ICookieHelper
    {
        private const string COOKIE_KEY = "UBER|ACCESSTOKEN";
        private  readonly IHttpContextAccessor _contextAccessor;
        public CookieHelper(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
        }
        public  AccessToken GetAccessToken()
        {

            var cookie = _contextAccessor.HttpContext.Request.Cookies[COOKIE_KEY];
            return cookie != null && !string.IsNullOrWhiteSpace(cookie)
                ? JsonConvert.DeserializeObject<AccessToken>(cookie)
                : null;
        }
        public  AccessToken GetAccessTokenTest()
        {
            var cookie = _contextAccessor.HttpContext.Request.Cookies[COOKIE_KEY];

            return cookie != null && !string.IsNullOrWhiteSpace(cookie)
                ? JsonConvert.DeserializeObject<AccessToken>(cookie)
                : null;
        }

        public  void SetAccessToken(AccessToken accessToken)
        {
            var accessTokenJson = JsonConvert.SerializeObject(accessToken);
            _contextAccessor.HttpContext.Response.Cookies.Append(COOKIE_KEY, accessTokenJson);

        }
        public  void SetAccessTokenTest(AccessToken accessToken, HttpResponse Response)
        {
            var accessTokenJson = JsonConvert.SerializeObject(accessToken);

            Response.Cookies.Append(COOKIE_KEY, accessTokenJson);
              
        }
    }
}
