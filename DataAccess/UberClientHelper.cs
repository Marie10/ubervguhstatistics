﻿
using System.Configuration;
using Uber.SDK;
using Uber.SDK.Models;
using UberVguhStatistics.Interfaces;

namespace UberVguhStatistics.DataAccess
{
    public class UberClientHelper : IUberClientHelper
    {
        public  UberAuthenticationClient GetAuth()
        {
            var clientId = ConfigurationManager.AppSettings["UBER.ClientId"];
            var clientSecret = ConfigurationManager.AppSettings["UBER.ClientSecret"];

            return new UberAuthenticationClient(clientId, clientSecret);
        }

        public  UberSandboxClient Get(string clientToken = null)
        {
            var serverToken = ConfigurationManager.AppSettings["UBER.ServerToken"];

            return string.IsNullOrWhiteSpace(clientToken)
                ? new UberSandboxClient(AccessTokenType.Server, serverToken)
                : new UberSandboxClient(AccessTokenType.Client, clientToken);
        }
    }
}
